import React from 'react';
import auth from 'solid-auth-client'

export class PostForm extends React.Component {
constructor(props) {
  super(props)

  this.state = {
    option1:'',
    option2:'',
    option3:''
  }
}

async componentDidMount() {

  const getRequest = await auth.fetch('https://maurex.inrupt.net/public/Interests.jsonld', {
    method: 'GET',
  })

  const values = await getRequest.json();

  this.setState({
    option1: values.Option_1,
    option2: values.Option_2,
    option3: values.Option_3,
  })
}

changeHandler = e => {
  this.setState({[e.target.name]: e.target.value})
}

submitHandler = async e => {
  e.preventDefault()

    const deleteRequest = await auth.fetch('https://maurex.inrupt.net/public/Interests.ttl', {
      method: 'DELETE',
    })

    const postRequest = await auth.fetch('https://maurex.inrupt.net/public/', {
        method: 'POST',

        headers:{
          slug: "Interests",
          'Content-Type': 'text/turtle'
        },

        body:JSON.stringify({

          "@context": "https://scherma.org",
          "@type":"Person",
          "name": "Maurex",

            "Option_1": this.state.option1,
            "Option_2": this.state.option2,
            "Option_3": this.state.option3

        })
      }
    )
}

  render(){
    const {option1, option2, option3} = this.state
console.log(this.getRequest)
    return (
      <div>
      <form onSubmit={this.submitHandler}>
        <fieldset>
          <legend>Categorieën:</legend>
          <select name="option1" className="custom" value={option1} onChange={this.changeHandler }>
            <option selected>None</option>
            <option value="0">Eten</option>
            <option value="1">Sport</option>
            <option value="2">Gaming</option>
          </select>
          <select name="option2" className="custom" value={option2} onChange={this.changeHandler }>
            <option selected>None</option>
            <option value="0">Eten</option>
            <option value="1">Sport</option>
            <option value="2">Gaming</option>
          </select>
          <select name="option3" className="custom" value={option3} onChange={this.changeHandler }>
            <option selected>None</option>
            <option value="0">Eten</option>
            <option value="1">Sport</option>
            <option value="2">Gaming</option>
          </select>
        </fieldset>
        <button type="submit" className="">Save</button>
      </form>
      </div>
    );
  }
}
export default PostForm
