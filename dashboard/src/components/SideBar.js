import React from 'react';
import './SideBar.css'

import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBars, faAd, faUser, faHeart, faGift, faDatabase, faHome } from '@fortawesome/free-solid-svg-icons';

class SideBarComponent extends React.Component {
  constructor(props) {
       super(props);

       this.state = { screenWidth: null };
       this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
   }

  toggleMenu = (event) => {
    document.getElementsByClassName('vertical-menu')[0].classList.toggle('nav-open');
  }

  componentDidMount() {
      this.updateWindowDimensions();
      window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
      window.removeEventListener("resize", this.updateWindowDimensions)
  }

  updateWindowDimensions() {
    if(window.innerWidth < 768) {
        document.getElementsByClassName('vertical-menu')[0].classList.remove('nav-open');
    } else {
      document.getElementsByClassName('vertical-menu')[0].classList.add('nav-open');
    }
    this.setState({ screenWidth: window.innerWidth });
  }

  render() {
    const { history } = this.props;
    const { location } = history;
    const { pathname } = location;
    console.log(pathname);

    return (
      <div className="view vertical-menu fixed">
        <div className="container">
          <div className="logo">
            <h1>AdRewards</h1>
            <FontAwesomeIcon className="mobile-nav" icon={faBars} />
          </div>
            <div className="menu">
                <a href="dashboard" className={pathname === '/dashboard' ? "active" : null}><FontAwesomeIcon icon={faHome} />Home</a>
                <a href="interests" className={pathname === '/interests' ? "active" : null}><FontAwesomeIcon icon={faHeart} />Interests</a>
                <a href="ads" className={pathname === '/ads' ? "active" : null}><FontAwesomeIcon icon={faAd} />Ads</a>
                <a href="rewards" className={pathname === '/rewards' ? "active" : null}><FontAwesomeIcon icon={faGift} />Rewards</a>
                <a href="data"className={pathname === '/data' ? "active" : null}><FontAwesomeIcon icon={faDatabase} />Your data</a>
                <a href="account" className={pathname === '/account' ? "active" : null}><FontAwesomeIcon icon={faUser} />Account</a>
            </div>
            <div className="copyright"></div>
          </div>
      </div>

    );
  }
}

export const SideBar = withRouter(SideBarComponent);
export default SideBar;
