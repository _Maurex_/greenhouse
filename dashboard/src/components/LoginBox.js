import React from 'react';
import './LoginBox.css'
import logo from '../assets/GreencaneFinal.png'
import { withRouter } from "react-router";
import {Link} from "react-router-dom";
import { LoginButton,LoggedOut} from '@solid/react';

import auth from 'solid-auth-client'

 class LoginBoxComponent extends React.Component {

async popupLogin() {
     let session = await auth.currentSession();
     let popupUri = 'https://solid.community/common/popup.html';
     if (!session)
       session = await auth.popupLogin({ popupUri });
     alert(`Logged in as ${session.webId}`);
   }


  render() {
    return (

      <div id="LoginBox" class="bg0 wsize1 bor1 p-l-45 p-r-45 p-t-50 p-b-18 p-lr-15-sm">
        <h3 className="l1-txt3 txt-center p-b-43">
          <img style={{height:"70px"}}src={logo} alt="GreenCane" />
        </h3>

        <div class="w-full validate-form" >
        <button className="flex-c-m size2 s1-txt2 how-btn1 trans-04" onClick={this.popupLogin}>Sign in as User</button>

          <LoggedOut>
              <LoginButton className="flex-c-m size2 s1-txt2 how-btn1 trans-04" popup="https://solid.community/common/popup.html">Sign in as Advertiser</LoginButton>
          </LoggedOut>
        </div>
      </div>
    );
  }
}


export const LoginBox = withRouter(LoginBoxComponent);
export default LoginBox;
