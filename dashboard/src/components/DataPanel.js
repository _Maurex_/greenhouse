import React from 'react';
import './DataPanel.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo, faAd, faWallet } from '@fortawesome/free-solid-svg-icons';

export class DataPanel extends React.Component {
  constructor(props){
    
    super(props);
    this.state = {
      count: 0,
      adcount: 0
      
    }
  }
  componentDidMount() {
    const stringCount = localStorage.getItem("count");
    const count = parseInt(stringCount, );

    const stringadcount = localStorage.getItem("adcount");
    const adcount = parseInt(stringadcount, );
    if (!isNaN(count)) {
      this.setState(() => ({ count }));
    }
    if (!isNaN(adcount)) {
      this.setState(() => ({ adcount }));
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count) {
      localStorage.setItem("count", this.state.count);
    }
    if (prevState.adcount !== this.state.adcount) {
      localStorage.setItem("adcount", this.state.adcount);
    }
  }
  handleAddOne() {
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      };
    });
  }
  handleMinusOne() {
    this.setState(prevState => {
      return {
        count: prevState.count - 1
      };
    });
  }
  handleReset() {
    this.setState(() => {
      return {
        count: 0
      };
    });
  }


  render() {
    return (
      <div class="icon-container" style={{fontSize: "6rem"}}>
        <div className="left">
            <h2 style={{fontSize: "1.5rem"}}>Bekeken video's</h2>
            <FontAwesomeIcon icon={faVideo} />
            <h3 style={{fontSize: "1.5rem", marginBottom: "30px"}}>{this.state.videocount}</h3>
        </div>
        <div className="center">
            <h2 style={{fontSize: "1.5rem",  whiteSpace: "nowrap"}}>Bekeken banner-ads</h2>
            <FontAwesomeIcon icon={faAd} />
            <h3 style={{fontSize: "1.5rem", marginBottom: "30px"}}>{this.state.adcount}</h3>
        </div>
          <div className="right">
            <h2 style={{fontSize: "1.5rem"}}>Munten balans</h2>
            <FontAwesomeIcon icon={faWallet} />
            <h3 style={{fontSize: "1.5rem", marginBottom: "30px"}}>{this.state.count}</h3>
            
        </div>
      </div>
        );
      }
    }
