import React from 'react';
import {SideBar} from '../components/SideBar';
import { LogoutButton, LoggedIn, Value} from '@solid/react';

export class Account extends React.Component {
  render() {
    return (
      <div>
        <SideBar />
        <section className="dashboard-page account">
          <header>
            <h1>Account</h1>
            <span>Account settings.</span>
          </header>

          <main>
            <form>
              <fieldset>
              <legend>Username:</legend>
              <input name="send-to" type="text" placeholder="Username" />
                <legend>Settings:</legend>
                <select name="option1" className="custom" >
                  <option selected disabled>Select maximum ads</option>
                  <option value="0">Default</option>
                  <option value="1">..</option>
                  <option value="2">..</option>
                </select>
              </fieldset>
              <button className="">Save</button>
              <LoggedIn>
          <LogoutButton class="flex-c-m size2 s1-txt2 how-btn1 trans-04">Log out</LogoutButton>
        </LoggedIn>
            </form>
          </main>
          
        </section>
      </div>
    );
  }
}
