import React from 'react';
import {SideBar} from '../components/SideBar';
import {PostForm} from '../components/PostForm';

export class Interests extends React.Component {

  render() {
    return (
      <div>
        <SideBar />
        <section className="dashboard-page interests">
        <header>
          <h1>Uw interesses</h1>
          <span>Kies een aantal categorieën waarin u geïnteresseerd bent.</span>
        </header>

        <main>
          <PostForm />
        </main>

        </section>
      </div>
    );
  }
}
