import React from 'react';
import {SideBar} from '../components/SideBar';
import './rewards-view.css'
import hamburger from '../assets/hamburger.jpg'
import Steamkey from '../assets/Steamkey.jpg'
//voor icon currency 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo, faAd, faWallet } from '@fortawesome/free-solid-svg-icons';
import ReactPlayer from 'react-player'


export class Rewards extends React.Component {

  constructor(props){
    
    super(props);
    this.state = {
      count: 0,
      adcount: 0,
      videocount: 0
    }
  }
  componentDidMount() {
    const stringCount = localStorage.getItem("count");
    const count = parseInt(stringCount, );

    if (!isNaN(count)) {
      this.setState(() => ({ count }));
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count) {
      localStorage.setItem("count", this.state.count);
      
    }
  }
  handleAddOne() {
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      };
    });
  }
  handleMinusOne() {
    this.setState(prevState => {
      return {
        count: prevState.count - 1
      };
    });
  }
  handleReset() {
    this.setState(() => {
      return {
        count: 0
      };
    });
  }
  

  decreasementFood = () =>{
    if (this.state.count <= 10){
      alert(`Teweinig punten`);
  }
  else{
    this.setState({count: this.state.count - 10})
  }
}
  
  decreasementGames = () =>{
    if (this.state.count <= 100){
      alert(`Teweinig punten`);
  }
  else{
    this.setState({count: this.state.count - 100})
  }

  
  }
  state = {ShowingAdsRow: true};
  state = {showingAds: true};
  
 
  state = {kortingsbonnen: true};
  state = {Eten: true};
  state = {Games: true};
  state = {Kleding: true};
  render() {
    const {kortingsbonnen} = this.state;
    const {Eten} = this.state;
    const {Games} = this.state;
    const {Kleding} = this.state;

    const {ShowingAdsRow} = this.state;
    const {showingAds} = this.state;

    
    return (
      <div>
        <SideBar />
        <section className="dashboard-page ads">
          <header>
            <h1>Rewards</h1>
            <span>Hier vindt u de verdiende coupons die u bij de betreffende winkels kunt inwisselen.</span>
            <h3 style={{fontSize: "3rem", marginBottom: "30px", marginLeft: "5px",}}>
              {this.state.count}
              <FontAwesomeIcon icon={faWallet} />
              </h3>
          </header>
          <button  onClick={() => this.setState({ kortingsbonnen: !kortingsbonnen })}>kortingsbonnen</button>
              {kortingsbonnen?
               <div class="btn-group">
               <button onClick={() => this.setState({ Eten: !Eten })}>Eten</button>
              {Eten?
              <div class="coupon">
              <img src={hamburger} alt="burger" style={{width:"200px" ,height: "200px"}}/>
              <div class="container-coupon" style={{backgroundColor:"white"}}>
              <h2><b>20% OFF YOUR PURCHASE</b></h2>
              <p>10 munten per aankoop</p>
              </div>
              <div class="container">
              <p>Promo Code: <span class="promo"></span></p>
              <p class="expire">Promo code wordt zichtbaar als u voldoende munten heeft.</p>
              <button onClick={this.decreasementFood}  className='counter'>Kopen</button>
              </div>
            </div>
              :null
              }
                <button onClick={() => this.setState({ Games: !Games })}>Games</button>
              {Games?
               <div class="coupon">
               <img src={Steamkey} alt="burger" style={{width:"200px" ,height: "200px"}}/>
               <div class="container-coupon" style={{backgroundColor:"white"}}>
               <h2><b>Free random steamkey</b></h2>
               <p>100 munten per aankoop</p>
               </div>
               <div class="container">
               <p>Promo Code: <span class="promo"></span></p>
               <p class="expire">Promo code wordt zichtbaar als u voldoende munten heeft.</p>
               <button onClick={this.decreasementGames}  className='counter'>Kopen</button>
               </div>
             </div>
              :null
              }
               <button onClick={() => this.setState({ GamKledinges: !Kleding })}>Kleding</button>
              {Kleding?
              <div>     
              {}
              
              </div>
              :null
              }

              
             </div> 
              :null
              }
          

        </section>
      </div>
    );
  }
}
