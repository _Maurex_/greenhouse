import React from 'react';
import {SideBar} from '../components/SideBar';

export class Data extends React.Component {
  render() {
    return (
      <div>
        <SideBar />
        <section className="dashboard-page data">
        <header>
          <h1>Uw data</h1>
          <span>Hier vindt u uw opgeslagen data.</span>
        </header>
        <main>
        </main>



        </section>
      </div>
    );
  }
}
