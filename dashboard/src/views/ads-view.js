import React, { useState } from 'react';
import {SideBar} from '../components/SideBar';
import ReactPlayer from 'react-player'
import ReactDOM from "react-dom";

//voor icon currency 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo, faAd, faWallet } from '@fortawesome/free-solid-svg-icons';

// import images hambuer
import hamburger from '../assets/hamburger.jpg'
// css
import './ads-vieuw.css'
import auth from 'solid-auth-client'





export class Ads extends React.Component 
{
  
 
   constructor(props){
    
    super(props);
    this.state = {
      count: 0,
      adcount: 0,
      vidcount: 0
      
    }
  }
  componentDidMount() {
    const stringCount = localStorage.getItem("count");
    const count = parseInt(stringCount, );

    const stringadcount = localStorage.getItem("adcount");
    const adcount = parseInt(stringadcount, );

    const stringvidcount = localStorage.getItem("vidcount");
    const vidcount = parseInt(stringvidcount, );
    if (!isNaN(count)) {
      this.setState(() => ({ count }));
    }
    if (!isNaN(adcount)) {
      this.setState(() => ({ adcount }));
    }
    if (!isNaN(vidcount)) {
      this.setState(() => ({ vidcount }));
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count) {
      localStorage.setItem("count", this.state.count);
    }
    if (prevState.adcount !== this.state.adcount) {
      localStorage.setItem("adcount", this.state.adcount);
    }
  }
  handleAddOne() {
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      };
    });
  }
  handleMinusOne() {
    this.setState(prevState => {
      return {
        count: prevState.count - 1
      };
    });
  }
  handleReset() {
    this.setState(() => {
      return {
        count: 0
      };
    });
  }
  

  // + 100 voor bekijken van een video
  increment = () =>{
    this.setState({count: this.state.count + 100})
    
    
  }
  // + 10 bekijken van een add
  increment1 = () =>{
    this.setState({count: this.state.count + 5})
    
  }
  incrementAddview = () =>{
    this.setState({adcount: this.state.adcount + 1})
    this.setState({count: this.state.count + 5})
    
  }
  incrementVidview = () =>{
    this.setState({Vidcount: this.state.Vidcount + 1})
    
  }
  
  // }

 

  state = {ShowingAdsRow: true};
  state = {showingAds: true};
  
 
  state = {ShowingVidRow: true};
  state = {showingVideo: true};
  state = {showingVideo1: true};


 
  render() {

  
    const {ShowingVidRow} = this.state;
    const {showingVideo} = this.state;
    const {showingVideo1} = this.state;

    const {ShowingAdsRow} = this.state;
    const {showingAds} = this.state;
    

  
 
 
    
    return (
      <div>
        <SideBar />
        <section className="dashboard-page ads">
          <header>
            <div>
            <h1>Advertenties</h1>
            <span>Hier kunt u advertenties bekijken waarvoor u wordt uitbetaald in munten.</span>
            <h3 style={{fontSize: "3rem", marginBottom: "30px", marginLeft: "5px",}}>
              {this.state.count}
              <FontAwesomeIcon icon={faWallet} />
              </h3>
              
              </div>
          </header>
          
          <div className="container"style={{fontSize: "3rem", marginBottom: "30px", marginLeft: "15px", marginTop: "15px"}}>
          
             <button  onClick={() => this.setState({ ShowingVidRow: !ShowingVidRow })}>Video list</button>
              {ShowingVidRow?
               <div class="btn-group">
               <button onClick={() => this.setState({ showingVideo: !showingVideo })}>Video's</button>
              {showingVideo?
              <div>     
              
              <ReactPlayer width="50%" fluid= "false" onClick={this.KeepCurrency} onEnded={this.increment} className='counter'  url='https://www.youtube.com/watch?v=Rzbd0nLg-zA'></ReactPlayer>
              </div>
              :null
              }
                <button onClick={() => this.setState({ showingVideo1: !showingVideo1 })}>Video's</button>
              {showingVideo1?
              <div>     
              {/* <ReactPlayer  onPlay={this.increment, this.KeepCurrency} className='counter' url='https://www.youtube.com/watch?v=Rzbd0nLg-zA'></ReactPlayer> */}
              <ReactPlayer width="50%" fluid= "false" onEnded={this.increment} onClick={this.incrementVidview} className='counter'  url='https://www.youtube.com/watch?v=FlTzfJTe7mE'></ReactPlayer>
              </div>
              :null
              }
               <button>Video 3</button>

              
             </div> 
              :null
              }
              
              <button onClick={() => this.setState({ ShowingAdsRow: !ShowingAdsRow })}>Ads List</button>
              {ShowingAdsRow?
               <div class="btn-group">
               <button onClick={() => this.setState({ showingAds: !showingAds })}>Banner ads</button>
              {showingAds?
              

              
              <div>
              <img onClick={this.increment1,this.incrementAddview}  className='counter' src={hamburger} alt="burger" style={{width:"25%"}}/>
              {/* <img onClick={this.increment1, this.KeepCurrency}  className='counter' src={hamburger} alt="burger" style={{width:"25%"}}/> */}
              </div>
              :null
            }
               <button>Ad 2</button>
               <button>AD 3</button>
             </div> 
              :null
              }
              

          </div>
        </section>
      </div>
    );
  }
}
