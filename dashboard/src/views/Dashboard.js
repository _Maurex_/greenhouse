import React from 'react';
import { SideBar } from '../components/SideBar';
import { DataPanel } from '../components/DataPanel';
import { LogoutButton, LoggedIn, Value} from '@solid/react';


export class Dashboard extends React.Component {

render() {
  return (
    <div>
      <SideBar />
      <section className="dashboard-page home">
        <header>
        <h1>Dashboard</h1>
        <span>
        <p>Welkom terug <Value src="user.name"/>, &nbsp;</p>
        <p>jouw WebID is <Value src="user"/></p>
        
        </span>
        </header>

        <main>
          <DataPanel />

        </main>
      </section>
    </div>
    );
  }
}
