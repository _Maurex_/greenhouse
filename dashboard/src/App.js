import React from 'react';
import {Dashboard} from './views/Dashboard';
import {LoginScreen} from './views/LoginScreen';
import {Ads} from './views/ads-view';
import {Rewards} from './views/rewards-view';
import {Account} from './views/account-view';
import {Interests} from './views/interests-view';
import {Data} from './views/data-view';
import './App.css';
import {LoggedIn, LoggedOut} from '@solid/react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  return (
    <Router>
      <LoggedOut>
          <Switch>
                <Route path= "/login">
                  <LoginScreen />
                </Route>
                <Redirect from="*" to="/login" />

          </Switch>
      </LoggedOut>

      <LoggedIn>
          <Switch>
                <Route path= "/dashboard">
                  <Dashboard />
                </Route>

                <Route path= "/ads">
                  <Ads />
                </Route>

                <Route path= "/rewards">
                  <Rewards />
                </Route>

                <Route path= "/data">
                  <Data />
                </Route>

                <Route path= "/account">
                  <Account />
                </Route>

                <Route path= "/interests">
                  <Interests />
                </Route>


                <Redirect from="*" to="/dashboard" />
            </Switch>
        </LoggedIn>
      </Router>
  );
}

export default App;
